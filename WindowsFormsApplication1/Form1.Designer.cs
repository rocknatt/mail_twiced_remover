﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.t_entree = new System.Windows.Forms.TextBox();
            this.t_sortie = new System.Windows.Forms.TextBox();
            this.t_rapport = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.b_exec = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Entrée (Séparé par un \";\")";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(330, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Sortie";
            // 
            // t_entree
            // 
            this.t_entree.Location = new System.Drawing.Point(12, 25);
            this.t_entree.MaxLength = 2147483647;
            this.t_entree.Multiline = true;
            this.t_entree.Name = "t_entree";
            this.t_entree.Size = new System.Drawing.Size(299, 358);
            this.t_entree.TabIndex = 1;
            // 
            // t_sortie
            // 
            this.t_sortie.Location = new System.Drawing.Point(333, 25);
            this.t_sortie.MaxLength = 2147483647;
            this.t_sortie.Multiline = true;
            this.t_sortie.Name = "t_sortie";
            this.t_sortie.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.t_sortie.Size = new System.Drawing.Size(285, 358);
            this.t_sortie.TabIndex = 1;
            // 
            // t_rapport
            // 
            this.t_rapport.Location = new System.Drawing.Point(15, 450);
            this.t_rapport.MaxLength = 2147483647;
            this.t_rapport.Multiline = true;
            this.t_rapport.Name = "t_rapport";
            this.t_rapport.Size = new System.Drawing.Size(603, 85);
            this.t_rapport.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 434);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Rapport";
            // 
            // b_exec
            // 
            this.b_exec.Location = new System.Drawing.Point(273, 389);
            this.b_exec.Name = "b_exec";
            this.b_exec.Size = new System.Drawing.Size(100, 23);
            this.b_exec.TabIndex = 3;
            this.b_exec.Text = "Executer >>";
            this.b_exec.UseVisualStyleBackColor = true;
            this.b_exec.Click += new System.EventHandler(this.b_exec_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 547);
            this.Controls.Add(this.b_exec);
            this.Controls.Add(this.t_rapport);
            this.Controls.Add(this.t_sortie);
            this.Controls.Add(this.t_entree);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox t_entree;
        private System.Windows.Forms.TextBox t_sortie;
        private System.Windows.Forms.TextBox t_rapport;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button b_exec;
    }
}

