﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void b_exec_Click(object sender, EventArgs e)
        {
            string str_input = t_entree.Text;
            List<string> final_mail_list = new List<string>();
            List<string> twiced_mail_list = new List<string>();

            var adr = str_input.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var mail in adr)
            {
                var str = mail.Trim();


                if (!final_mail_list.Contains(str))
                {
                    final_mail_list.Add(str);
                }
                else
                {
                    twiced_mail_list.Add(str);
                }
            }

            string final_mail_str = "";
            final_mail_str += final_mail_list.Count + " mail(s) au total.\r\n-----------------------------------\r\n\r\n";
            foreach (string mail in final_mail_list)
            {
                final_mail_str += mail + "; ";
            }

            string twiced_mail_str = "";
            twiced_mail_str += twiced_mail_list.Count + " doublon(s) repéré.\r\n-----------------------------------------\r\n\r\n";

            foreach (string mail in twiced_mail_list)
            {
                twiced_mail_str += " > " + mail + "\r\n";
            }

            t_sortie.Text = final_mail_str;
            t_rapport.Text = twiced_mail_str;
        }
    }
}
